FROM php:7.1-apache
ARG COMPOSER_VERSION="1.6.5"

RUN apt-get update
RUN curl https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar > /usr/local/bin/composer && chmod +x /usr/local/bin/composer
RUN apt-get -y install git
RUN apt-get -y install unzip
RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip
